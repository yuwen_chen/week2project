# week2project
This is the repository for IDS721 week2 mini project.

## Content
This repository contains all the intermediate files to demonstrate the successfully completing all the requirements.

## Results demonstrate `cargo lambda watch` is running
![image](image_1.png)

## `cargo lambda build --release`
![image](image_2.png)

## Deploy and Invoke
![image](image_3.png)
![image](image_4.png)
